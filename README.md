# Goal

This project is a PoC to learn how to use microbit.

# Scripts

## Temperature

It shows the microbit temperature

## Counter

If press A button add 1 to counter and show it.
If press B button substract 1 to counter and show it.
