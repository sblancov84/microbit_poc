from microbit import *

counter = 0

while True:
    if button_a.was_pressed():
        if counter < 9:
            counter += 1
    elif button_b.was_pressed():
        if counter > 0:
            counter -= 1
    display.show(str(counter))
