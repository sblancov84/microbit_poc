activate:
	pipenv shell

install-dependencies:
	pipenv install

flash-counter:
	uflash counter.py

flash-temperature:
	uflash temperature.py
